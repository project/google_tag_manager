<?php

/**
 * @file
 * google_tag_manager.admin.inc
 */

/**
 * Google Tag manager admin settings form.
 */
function google_tag_manager_admin_settings_form() {
  $form['account']['google_tag_manager_container_id'] = array(
    '#title' => t('Container ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('google_tag_manager_container_id', 'GTM-'),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('You’ll usually have one container for each website that you manage. A container includes the tracking tags (e.g. Google Analytics, AdWords Conversion Tracking tags) that you want to use on the website. This ID is in the form of GTM-XXXX. To get a Container ID, <a href="@analytics">register your site with Google Tag Manager</a>, or if you already have registered your site, go to your Google Tag Manager Settings area for the container to see the Container ID. <a href="@webpropertyid">Find more information in the documentation</a>.', array('@analytics' => 'http://www.google.co.uk/tagmanager/', '@webpropertyid' => url('https://developers.google.com/tag-manager/devguide', array('fragment' => 'webProperty')))),
  );

  // Render the role overview.
  $form['roles']['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#description' => t('Select the roles for which tracking is enabled.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['roles']['role_vis_settings']['google_tag_manager_visibility_roles'] = array(
    '#type' => 'radios',
    '#title' => t('Add tracking for specific roles'),
    '#options' => array(
      t('Add to the selected roles only'),
      t('Add to every role except the selected ones'),
    ),
    '#default_value' => variable_get('google_tag_manager_visibility_roles', 0),
  );

  $role_options = array_map('check_plain', user_roles());

  $form['roles']['role_vis_settings']['google_tag_manager_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => variable_get('google_tag_manager_roles', array()),
    '#options' => $role_options,
    '#description' => t('If none of the roles are selected, all users will be tracked. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Form submission callback for google_tag_manager_admin_settings_form().
 */
function google_tag_manager_admin_settings_form_submit($form, &$form_state) {
  variable_set('google_tag_manager_container_id', $form_state['values']['google_tag_manager_container_id']);
  variable_set('google_tag_manager_visibility_roles', $form_state['values']['google_tag_manager_visibility_roles']);
  variable_set('google_tag_manager_roles', $form_state['values']['google_tag_manager_roles']);
}
