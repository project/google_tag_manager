# CONFIGURATION

The module doesn't currently have UI for setting affected
forms fields that'll be included in the GTM dataLayer (single use case).
to set this configuration use `variable_set('gtm_forms_datalayer', $data);`
from devel php or set the variable using drush. The data array should consist of array
of form fields for each affected form keyed by the form ID. each field is an array
of field parents corresponding to structure in result `$form_state['values']` where
the key is the name of the variable in dataLayer object sent to GTM.

Example:

```
variable_set('gtm_forms_datalayer', [
  'webform_client_form_X' => [
    'fields' => [
      'dataLayerName1' => ['some_form_field1'],
      'dataLayerName2' => ['some_form_parent', 'some_form_field2'],
    ],
  ],
  'form_id_2' => ...
]);
```
